function MovementDetector(lightBulbController, thresholdInMilliSeconds) {
  this.lights = lightBulbController;
  this.thresholdInMS = thresholdInMilliSeconds;
}

MovementDetector.prototype.stopTimeoutFunction = function() {
  if (typeof this.timeoutFunction !== 'undefined') {
    window.clearTimeout(this.timeoutFunction);
    delete this.timeoutFunction
  }
}

MovementDetector.prototype.onEventReceived = function() {
  this.stopTimeoutFunction();
  this.lights.turnOnGreen();
  this.timeoutFunction = window.setTimeout(this.noEventReceived.bind(this), this.thresholdInMS);
}

MovementDetector.prototype.noEventReceived = function() {
  this.lights.turnOnRed();
}

function LightBulbController(redLightImage, greenLightImage) {
  this.redLight = redLightImage;
  this.greenLight = greenLightImage;
}

LightBulbController.prototype.turnOnRed = function() {
  this.redLight.src = "images/red.png";
  this.greenLight.src = "images/grey.png";
}

LightBulbController.prototype.turnOnGreen = function() {
  this.redLight.src = "images/grey.png";
  this.greenLight.src = "images/green.png";
}

LightBulbController.prototype.turnOff = function() {
  this.redLight.src = "images/grey.png";
  this.greenLight.src = "images/grey.png";
}
