package com.allianz.itada.demo.aac.emulator;

import java.util.Date;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensorEmulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensorEmulatorApplication.class, args);
        
        String topic        = "aac/mqtt";
        String content = "{\"device\":\"2182918\",\"status\":\"motion\",\"timestamp\":\"%1$tQ\"}";
        int qos             = 2;
        String broker       = "tcp://localhost:1883";
        String clientId     = "AAC_Demo_SensorEmulator";

        try {
            MqttClient sampleClient = new MqttClient(broker, clientId);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected");
            Date now = new Date();
            System.out.println("Publishing message: "+String.format(content, now));
            MqttMessage message = new MqttMessage(String.format(content, now).getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
            System.exit(0);
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }

    }
}
