package com.allianz.itada.demo.acc.api;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SensorRepository extends MongoRepository<SensorData, String> {

	public List<SensorData> findAll();
	public List<SensorData> findByDevice(String device );
	public List<SensorData> findByStatus( String status );
	
}
