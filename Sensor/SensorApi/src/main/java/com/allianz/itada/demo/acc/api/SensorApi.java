package com.allianz.itada.demo.acc.api;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sensors")
public class SensorApi {

	@Autowired	
	private MongoTemplate mongoTemplate;
	
	private final static long TEN_MINUTES = 60000000L;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<SensorData> getSensorData(@RequestParam(value="device") String device){
		long tenMinutesAgo = System.currentTimeMillis()-TEN_MINUTES;
		Query query = new Query(Criteria.where("device").is(device).and("timestamp").gte(new Date(tenMinutesAgo))).with(new Sort(Sort.Direction.DESC, "timestamp"));		
		List<SensorData> data = mongoTemplate.find(query, SensorData.class);
		System.out.println("Sensor Data are = "+data);
		return data;
	}
}
