package com.allianz.itada.demo.acc.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="sensor")
public class SensorData {
    
	final static String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz";
    
	public SensorData(){
		super();
	}
	
	public SensorData(String device, String status, Date timestamp) {
		super();
		this.device = device;
		this.status = status;
		this.timestamp = timestamp;
	}

	@Id 
	private String id;
	public String device;
	public String status;
	public Date timestamp;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDevice() {
		return device;
	}
	
	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("Id = ");
		buffer.append(id);
		buffer.append(" , device = ");
		buffer.append(device);
		buffer.append(" , status = ");
		buffer.append(status);
		buffer.append(" , timestamp = ");
		buffer.append((new SimpleDateFormat(ISO_FORMAT)).format(timestamp));
		return buffer.toString();
	}

	
	
}
