package com.allianz.itada.demo.acc.api;

import java.net.UnknownHostException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.Mongo;

@Configuration
public class SensorApiConfig {

	public @Bean Mongo mongo() throws UnknownHostException {
		Mongo mongo = new Mongo("127.0.0.1");
		return mongo;
	}
	
	public @Bean MongoTemplate mongoTemplate() throws UnknownHostException {
		return new MongoTemplate(mongo(), "test");
	}
			
		
}
