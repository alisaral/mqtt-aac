package com.allianz.itada.demo.aac.sensor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SensorString2PojoTransformerTest {

    final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz";
    
	@Test
	public void testConvert() {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(ISO_FORMAT);
			Date d = formatter.parse("2015-10-08T13:45:21.602 CEST");
			String test = "{\"device\":\"2182918\",\"status\":\"motion\",\"timestamp\":\""+d.getTime()+"\"}";
			SensorString2PojoTransformer transformer = new SensorString2PojoTransformer();
			SensorData data = transformer.convert(test);
			System.out.println("SensorData = "+data);
			System.out.println("JSON Sensor Data ="+(new ObjectMapper()).writeValueAsString(data));
			assertEquals("2182918", data.getDevice());
			assertEquals("motion", data.getStatus());
			assertEquals( d , data.getTimestamp());			
		} catch (Exception e) 
		{
			e.printStackTrace();
			fail();
		}
	}
}
