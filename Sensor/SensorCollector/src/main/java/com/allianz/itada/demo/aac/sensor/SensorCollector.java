package com.allianz.itada.demo.aac.sensor;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public class SensorCollector {
    public static void main(String[] args) throws Exception {
    	ConfigurableApplicationContext ctx =null;
    	try{
    		ctx = new SpringApplication("/mqtt/mqtt.xml").run(args);
    		System.in.read();
    	}finally
    	{
    		if(ctx != null) ctx.close();    		
    	}
    }
}
