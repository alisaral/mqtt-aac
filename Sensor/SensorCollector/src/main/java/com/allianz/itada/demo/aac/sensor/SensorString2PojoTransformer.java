package com.allianz.itada.demo.aac.sensor;

import com.fasterxml.jackson.databind.ObjectMapper;


public class SensorString2PojoTransformer {

	public SensorData convert(String sensorString) throws Exception {
		return (new ObjectMapper()).readValue(sensorString.getBytes(), SensorData.class );
	}
	
}
