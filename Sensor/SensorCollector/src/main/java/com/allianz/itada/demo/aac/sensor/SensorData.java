package com.allianz.itada.demo.aac.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"device","status","timestamp"})
public class SensorData {
    
	final static String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz";
    
	public SensorData(){
		super();
	}
	
	public SensorData(String device, String status, Date timestamp) {
		super();
		this.device = device;
		this.status = status;
		this.timestamp = timestamp;
	}

	@JsonProperty ("device")
	public String device;
	@JsonProperty ("status")
	public String status;
	@JsonProperty ("timestamp")
	public Date timestamp;
	
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty ("device")
	public String getDevice() {
		return device;
	}
	
	@JsonProperty ("device")
	public void setDevice(String device) {
		this.device = device;
	}
	
	@JsonProperty ("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty ("status")
	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonProperty ("timestamp")
	public Date getTimestamp() {
		return timestamp;
	}

	@JsonProperty ("timestamp")
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("device = ");
		buffer.append(device);
		buffer.append(" status = ");
		buffer.append(status);
		buffer.append(" timestamp = ");
		buffer.append((new SimpleDateFormat(ISO_FORMAT)).format(timestamp));
		return buffer.toString();
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
	
	
}
